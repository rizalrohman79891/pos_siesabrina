<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
        $this->loginCheck();
	}

	public function index()
	{
		
		$this->load->view("superadmin/view-index");
	}

	public function data_barang()
	{
		$data_barang = $this->ModelUniv->read("barang");
		$this->load->view("superadmin/view-data-barang", ['data_barang' => $data_barang]);
	}

	public function tambah_data_barang()
	{
		$data = [
			'barcode' => $this->input->post("barcode"),
			'nama_barang' => $this->input->post("nama_barang"),
			'harga_beli' => $this->input->post("harga_beli"),
			'harga_jual_umum' => $this->input->post("harga_jual_umum"),
			'harga_jual_grosir' => $this->input->post("harga_jual_grosir"),
			'harga_jual_khusus' => $this->input->post("harga_jual_khusus"),
			'stok_toko' => $this->input->post("stok_toko"),
			'stok_gudang' => $this->input->post("stok_gudang"),
			'kategori' => $this->input->post("kategori"),
		];

		$this->ModelUniv->create($data, 'barang');
		return redirect(base_url("superadmin/data_barang"));
	}

	public function delete_data_barang($id)
	{
		$this->ModelUniv->delete($id, 'barang');
		return redirect(base_url("superadmin/data_barang"));		
	}

	public function edit_data_barang()
	{
		$where = ['id_barang' => $this->input->post("id_barang")];
		$data = [
			'barcode' => $this->input->post("barcode"),
			'nama_barang' => $this->input->post("nama_barang"),
			'harga_beli' => $this->input->post("harga_beli"),
			'harga_jual_umum' => $this->input->post("harga_jual_umum"),
			'harga_jual_grosir' => $this->input->post("harga_jual_grosir"),
			'harga_jual_khusus' => $this->input->post("harga_jual_khusus"),
			'stok_toko' => $this->input->post("stok_toko"),
			'stok_gudang' => $this->input->post("stok_gudang"),
		];

		$this->ModelUniv->update($where, 'barang', $data);
		return redirect(base_url("superadmin/data_barang"));
	}

	public function data_pelanggan()
	{
		$data_pelanggan = $this->ModelUniv->read("pelanggan");
		$this->load->view("superadmin/view-data-pelanggan", ['data_pelanggan' => $data_pelanggan]);
	}

	public function delete_data_pelanggan($id)
	{
		$this->ModelUniv->delete($id, 'pelanggan');
		return redirect(base_url("superadmin/data_pelanggan"));		
	}

	public function tambah_data_pelanggan()
	{
		$data = [
			'nama_pelanggan' => $this->input->post("nama_pelanggan"),
			'no_tlp' => $this->input->post("no_tlp"),
			'alamat' => $this->input->post("alamat"),
		];

		$this->ModelUniv->create($data, 'pelanggan');
		return redirect(base_url("superadmin/data_pelanggan"));
	}

	public function edit_data_pelanggan()
	{
		$where = ['id_pelanggan' => $this->input->post("id_pelanggan")];
		$data = [
			'nama_pelanggan' => $this->input->post("nama_pelanggan"),
			'no_tlp' => $this->input->post("no_tlp"),
			'alamat' => $this->input->post("alamat"),
		];

		$this->ModelUniv->update($where, 'pelanggan', $data);
		return redirect(base_url("superadmin/data_pelanggan"));
	}

	public function data_supplier()
	{
		$data_supplier = $this->ModelUniv->read("supplier");
		$this->load->view("superadmin/view-data-supplier", ['data_supplier' => $data_supplier]);
	}

	public function tambah_data_supplier()
	{
		$data = [
			'nama_supplier' => $this->input->post("nama_supplier"),
			'no_tlp' => $this->input->post("no_tlp"),
			'alamat' => $this->input->post("alamat"),
		];

		$this->ModelUniv->create($data, 'supplier');
		return redirect(base_url("superadmin/data_supplier"));
	}

	public function delete_data_supplier($id)
	{
		$this->ModelUniv->delete($id, 'supplier');
		return redirect(base_url("superadmin/data_supplier"));		
	}

	public function edit_data_supplier()
	{
		$where = ['id_supplier' => $this->input->post("id_supplier")];
		$data = [
			'nama_supplier' => $this->input->post("nama_supplier"),
			'no_tlp' => $this->input->post("no_tlp"),
			'alamat' => $this->input->post("alamat"),
		];

		$this->ModelUniv->update($where, 'supplier', $data);
		return redirect(base_url("superadmin/data_supplier"));
	}

	public function data_sales()
	{
		$data_sales = $this->ModelUniv->read("sales");
		$this->load->view("superadmin/view-data-sales", ['data_sales' => $data_sales]);
	}

	public function tambah_data_sales()
	{
		$data = [
			'nama_sales' => $this->input->post("nama_sales"),
			'no_tlp' => $this->input->post("no_tlp"),
			'alamat' => $this->input->post("alamat"),
		];

		$this->ModelUniv->create($data, 'sales');
		return redirect(base_url("superadmin/data_sales"));
	}

	public function cetak_barcode($barcode)
	{
		$generator = new Picqer\Barcode\BarcodeGeneratorJPG();
		file_put_contents('public/barcode/barcode.jpg', $generator->getBarcode($barcode, $generator::TYPE_UPC_A));
		 $data_barcode = $this->db->get_where('barang', array('barcode =' => $barcode))->result();

		$this->load->view("superadmin/cetak_barcode", ['data_barcode' => $data_barcode]);

	}
}
