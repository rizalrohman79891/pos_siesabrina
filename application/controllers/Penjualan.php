<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('pembelian_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('string');
        $this->loginCheck();
    }

    public function index()
    {
        $data_pelanggan = $this->ModelUniv->read("pelanggan");
        $data_barang = $this->ModelUniv->read("barang");
        $no_faktur = 'PNJ-'.date('YmdHis').random_string('numeric', 4);
        $rekening = $this->ModelUniv->read("rekening");

		$this->load->view("superadmin/view-tambah-penjualan", [
            'data_pelanggan' => $data_pelanggan, 
            'data_barang' => $data_barang, 
            'no_faktur' => $no_faktur,
            'rekening' => $rekening,
        ]);
    }

    public function create()
    {
        $data_supplier = $this->ModelUniv->read("supplier");
        $data_barang = $this->ModelUniv->read("barang");
        $no_faktur = 'PNJ-'.date('YmdHis').random_string('numeric', 4);
        $rekening = $this->ModelUniv->read("rekening");

		$this->load->view("superadmin/view-tambah-pembelian", [
            'data_supplier' => $data_supplier, 
            'data_barang' => $data_barang, 
            'no_faktur' => $no_faktur,
            'rekening' => $rekening,
        ]);
    }

    public function process()
    {
        // echo json_encode($this->input->post());
        $this->db->trans_start();
        $data = [
			'no_faktur' => $this->input->post("no_faktur"),
			'tanggal' => date('Y-m-d H:i:s'),
			'id_pelanggan' => $this->input->post("id_pelanggan"),
			'total' => $this->input->post("total"),
			'total_bayar' => $this->input->post("total_bayar"),
			'jenis_bayar' => $this->input->post("jenis_bayar"),
			'kembalian' => $this->input->post("kembalian"),
			'keterangan' => $this->input->post("keterangan"),   
			'id_user' => $this->session->userdata('id_user'),  
			'total_diskon' => $this->input->post("total_diskon"),
			'status' => 'SUCCESS',   
		];

		$this->ModelUniv->create($data, 'penjualan');

        $this->db->select('*');
        $this->db->from('penjualan');
        $this->db->order_by('created_at', 'DESC');
        $this->db->limit(1);

        $penjualan = $this->db->get()->result();

        for ($i = 0; $i < count($this->input->post('id_barang')); $i++) {
            $detail = [
                'id_barang' => $this->input->post("id_barang")[$i],
                'id_penjualan' => $penjualan[0]->id_penjualan,
                'quantity' => $this->input->post('jumlah')[$i],
                'harga' => $this->input->post('harga_jual')[$i],
                'subtotal' => $this->input->post('subtotal')[$i],
                'diskon' => $this->input->post('diskon')[$i],
                'keterangan' => ''
            ];
            $this->ModelUniv->create($detail, 'penjualan_detail');

            $this->db->select('*');
            $this->db->from('barang');
            $this->db->where('id_barang', $detail['id_barang']);
            
            $barang = $this->db->get()->row();

            $update_stok = [
                'stok_toko' => $barang->stok_toko -= $detail['quantity']
            ];

            $this->ModelUniv->update([
                'id_barang' => $detail['id_barang']
            ], 'barang', $update_stok);
        } 

        if ($data['jenis_bayar'] == 'TRANSFER') {
            $penjualan_transfer = [
                'id_penjualan' => $penjualan[0]->id_penjualan,
                'total_transfer' => $this->input->post('total'),
                'id_rekening' => $this->input->post('id_rekening')
            ];
            $this->ModelUniv->create($penjualan_transfer, 'penjualan_transfer');
        } else if($data['jenis_bayar'] == 'CREDIT') {
            $hutang = [
                'id_penjualan' => $penjualan[0]->id_penjualan,
                'total_hutang' => $this->input->post('total'),
            ];
            $this->ModelUniv->create($hutang, 'hutang');
        }

        $this->db->trans_complete();

		return redirect(base_url("penjualan/cetak_struk/".$penjualan[0]->id_penjualan));
    }

    public function cetak_struk($penjualan_id)
    {
        // $data_penjualan = $this->ModelUniv->ambil_data_penjualan_Terakhir();
        // $this->load->library('pdf');
        // $this->pdf->setPaper('A4', 'potrait');
        // $this->pdf->filename = "laporan-stok-kwluE.pdf";
        // $this->pdf->load_view('superadmin/export-laporan-stok-keluar', ['data_penjualan' => $data_penjualan]);

        $this->db->select('penjualan.total as total, penjualan.tanggal as tanggal_transaksi, 
        penjualan.id_penjualan, penjualan.no_faktur as nota, user.nama as nama_kasir, penjualan.total_diskon as total_diskon,
        penjualan.total_bayar as total_bayar, penjualan.kembalian as kembalian');
        $this->db->join('user', 'penjualan.id_user = user.id_user');
        $this->db->where('penjualan.id_penjualan', $penjualan_id);
        $penjualan = $this->db->get('penjualan')->result();
        foreach ($penjualan as $key => &$value) {
            $this->db->select('penjualan_detail.*, barang.nama_barang');
            $this->db->from('penjualan_detail');
            $this->db->join('penjualan', 'penjualan.id_penjualan = penjualan_detail.id_penjualan');
            $this->db->join('barang', 'penjualan_detail.id_barang = barang.id_barang');
            $this->db->where('penjualan_detail.id_penjualan', $value->id_penjualan);
            $value->penjualan_detail = $this->db->get()->result();
        }
        $data['penjualan'] = $penjualan[0];

        // $this->db->select('settings.*');
        // $this->db->limit(1);
        // $this->db->from('settings');
        // $data['settings'] = $this->db->get()->row_array();

        $this->load->view('superadmin/view-struk-penjualan', $data);

    }
}