<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('mutasi_kas_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->loginCheck();
    }

    public function laporan_stok_masuk()
    {
		// Laporan Stok Masuk
    	// Dapatkan data transaksi pembelian terbaru

    	// $data_pembelian = $this->ModelUniv->select_data_terbaru('pembelian_detail', 'DESC');

    	$data_pembelian = $this->ModelUniv->join_laporan_stok_masuk();
    	// hitung quantity dan jadikan sebagai stok masuk
    	$this->load->view("superadmin/view-laporan-stok-masuk", ['data_pembelian' => $data_pembelian]);
    }

    public function export_laporan_stok_masuk()
    {
  		//$this->load->model('siswa_model');
		// $data['siswa'] = $this->siswa_model->getData();
		$data_pembelian = $this->ModelUniv->join_laporan_stok_masuk();
		$this->load->library('pdf');
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-stok-masuk.pdf";
		$this->pdf->load_view('superadmin/export-laporan-stok-masuk', ['data_pembelian' => $data_pembelian]);
    }

    public function laporan_stok_keluar()
    {
		// Laporan Stok Masuk
    	// Dapatkan data transaksi pembelian terbaru

    	// $data_pembelian = $this->ModelUniv->select_data_terbaru('pembelian_detail', 'DESC');

    	$data_penjualan = $this->ModelUniv->join_laporan_stok_keluar();
    	// hitung quantity dan jadikan sebagai stok keluar
    	$this->load->view("superadmin/view-laporan-stok-keluar", ['data_penjualan' => $data_penjualan]);
    }

    public function export_laporan_stok_keluar()
    {
  		//$this->load->model('siswa_model');
		// $data['siswa'] = $this->siswa_model->getData();
		$data_penjualan = $this->ModelUniv->join_laporan_stok_keluar();
		$this->load->library('pdf');
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-stok-kwluE.pdf";
		$this->pdf->load_view('superadmin/export-laporan-stok-keluar', ['data_penjualan' => $data_penjualan]);
    }

    public function laporan_hutang()
    {
		// Join Penjualan, Pelanggan, hutang
		$data_hutang = $this->ModelUniv->ambil_data_laporan_hutang();
		$this->load->view("superadmin/view-laporan-hutang", ['data_hutang' => $data_hutang]);
    }

    public function export_laporan_hutang()
    {
  		//$this->load->model('siswa_model');
		// $data['siswa'] = $this->siswa_model->getData();
		$data_hutang = $this->ModelUniv->ambil_data_laporan_hutang();
		$this->load->library('pdf');
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-hutang.pdf";
		$this->pdf->load_view('superadmin/export-laporan-hutang', ['data_hutang' => $data_hutang]);
    }

    public function laporan_piutang()
    {
		// Join Penjualan, Pelanggan, hutang
		$data_piutang = $this->ModelUniv->ambil_data_laporan_piutang();
		$this->load->view("superadmin/view-laporan-piutang", ['data_piutang' => $data_piutang]);
    }

    public function export_laporan_piutang()
    {
  		//$this->load->model('siswa_model');
		// $data['siswa'] = $this->siswa_model->getData();
		$data_piutang = $this->ModelUniv->ambil_data_laporan_piutang();
		$this->load->library('pdf');
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-piutang.pdf";
		$this->pdf->load_view('superadmin/export-laporan-piutang', ['data_piutang' => $data_piutang]);
    }

    // public function best_seller()
    // {
    // 	// Hitung jumlah rows id_pelanggan di tabel penjualan
    // 	// join tabel pelangagn dan penjualan
    	
    // 	$data_best_seller => $this->ModelUniv->ambil_data_best_seller();
    // }

    public function pembayaran_hutang()
    {
        // Join bayar_hutang, Pelanggan, hutang
        $data_hutang = $this->ModelUniv->ambil_data_laporan_bayar_hutang();
        $this->load->view("superadmin/view-laporan-pembayaran-hutang", ['data_hutang' => $data_hutang]);
    }

    public function export_pembayaran_hutang()
    {
        //$this->load->model('siswa_model');
        // $data['siswa'] = $this->siswa_model->getData();
        $data_laporan_bayar_hutang = $this->ModelUniv->ambil_data_laporan_bayar_hutang();
        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan-bayar-hutang.pdf";
        $this->pdf->load_view('superadmin/export-laporan-bayar-hutang', ['data_laporan_bayar_hutang' => $data_laporan_bayar_hutang]);
    }
}
