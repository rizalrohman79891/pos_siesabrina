<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mutasi_kas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mutasi_kas_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->loginCheck();
    }

    public function index()
    {
        $data_mutasi_kas = $this->ModelUniv->read("mutasi_kas");
		$this->load->view("superadmin/view-mutasi-kas", ['data_mutasi_kas' => $data_mutasi_kas]);
    }

	public function tambah_data_mutasi_kas()
	{
		$data = [
			'jumlah' => $this->input->post("jumlah"),
			'tanggal' => date('Y-m-d H:i', strtotime($this->input->post("tanggal"))).':'.date('s'),
			'jenis' => $this->input->post("jenis"),   
			'keterangan' => $this->input->post("keterangan"),   
		];

		$this->ModelUniv->create($data, 'mutasi_kas');
		return redirect(base_url("mutasi_kas"));
	}

	public function delete_data_mutasi_kas($id)
	{
		$this->ModelUniv->delete($id, 'mutasi_kas');
		return redirect(base_url("mutasi_kas"));		
	}

	public function edit_data_mutasi_kas()
	{
		$where = ['id_mutasi_kas' => $this->input->post("id_mutasi_kas")];
		$data = [
			'jumlah' => $this->input->post("jumlah"),
			'tanggal' => date('Y-m-d H:i', strtotime($this->input->post("tanggal"))).':'.date('s'),
			'jenis' => $this->input->post("jenis"),   
			'keterangan' => $this->input->post("keterangan"), 
		];

		$this->ModelUniv->update($where, 'mutasi_kas', $data);
		return redirect(base_url("mutasi_kas"));
	}

}