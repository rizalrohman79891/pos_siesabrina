<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');

    }

    public function index()
    {
        $this->load->view('superadmin/view-login');
    }

    public function process()
    {
		$rules = $this->login_model->rules();
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == FALSE){
			return $this->load->view('superadmin/view-login');
		}

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($this->login_model->login($username, $password)){
			redirect('/');
		} else {
			$this->session->set_flashdata('login_error', 'Username atau Password Salah!');
		}

        return $this->load->view('superadmin/view-login');
    }

    public function logout()
	{
		$this->login_model->logout();
		redirect('/login');
	}

}