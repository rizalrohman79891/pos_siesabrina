<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('string');
        $this->loginCheck();
    }

    public function index()
    {
        // $data_pembelian = $this->ModelUniv->read("pembelian");

        $this->db->select('pembelian.*, supplier.id_supplier, supplier.nama_supplier');
        $this->db->from('pembelian');
        $this->db->join('supplier', 'supplier.id_supplier = pembelian.id_supplier');
        $this->db->order_by('pembelian.created_at', 'DESC');
        
        $data_pembelian = $this->db->get()->result();

		$this->load->view("superadmin/view-pembelian", ['data_pembelian' => $data_pembelian]);
    }

    public function create()
    {
        $data_supplier = $this->ModelUniv->read("supplier");
        $data_barang = $this->ModelUniv->read("barang");
        $no_faktur = 'PMB-'.date('YmdHis').random_string('numeric', 4);
        $rekening = $this->ModelUniv->read("rekening");

		$this->load->view("superadmin/view-tambah-pembelian", [
            'data_supplier' => $data_supplier, 
            'data_barang' => $data_barang, 
            'no_faktur' => $no_faktur,
            'rekening' => $rekening,
        ]);
    }

    public function process()
    {
        // echo json_encode($this->input->post());
        $this->db->trans_start();
        $data = [
			'no_faktur' => $this->input->post("no_faktur"),
			'tanggal' => $this->input->post("tanggal").' '.date('i'),
			'id_supplier' => $this->input->post("id_supplier"),
			'total' => $this->input->post("total"),
			'total_bayar' => $this->input->post("total_bayar"),
			'jenis_bayar' => $this->input->post("jenis_bayar"),
			'kembalian' => $this->input->post("kembalian"),
			'keterangan' => $this->input->post("keterangan"),   
			'id_user' => $this->session->userdata('id_user'),   
			'diskon' => 0,   
			'status' => 'SUCCESS',   
		];

		$this->ModelUniv->create($data, 'pembelian');

        $this->db->select('*');
        $this->db->from('pembelian');
        $this->db->order_by('created_at', 'DESC');
        $this->db->limit(1);

        $pembelian = $this->db->get()->result();

        for ($i = 0; $i < count($this->input->post('id_barang')); $i++) {
            $detail = [
                'id_barang' => $this->input->post("id_barang")[$i],
                'id_pembelian' => $pembelian[0]->id_pembelian,
                'quantity' => $this->input->post('jumlah')[$i],
                'harga' => $this->input->post('harga_jual')[$i],
                'subtotal' => $this->input->post('subtotal')[$i],
                'keterangan' => ''
            ];
            $this->ModelUniv->create($detail, 'pembelian_detail');

            $this->db->select('*');
            $this->db->from('barang');
            $this->db->where('id_barang', $detail['id_barang']);
            
            $barang = $this->db->get()->row();

            $update_stok = [
                'stok_toko' => $barang->stok_toko += $detail['quantity']
            ];

            $this->ModelUniv->update([
                'id_barang' => $detail['id_barang']
            ], 'barang', $update_stok);
        } 

        if ($data['jenis_bayar'] == 'TRANSFER') {
            $pembelian_transfer = [
                'id_pembelian' => $pembelian[0]->id_pembelian,
                'total_transfer' => $this->input->post('total'),
                'id_rekening' => $this->input->post('id_rekening')
            ];
            $this->ModelUniv->create($pembelian_transfer, 'pembelian_transfer');
        } else if($data['jenis_bayar'] == 'CREDIT') {
            $piutang = [
                'id_pembelian' => $pembelian[0]->id_pembelian,
                'total_piutang' => $this->input->post('total'),
            ];
            $this->ModelUniv->create($piutang, 'piutang');
        }

        $this->db->trans_complete();

		return redirect(base_url("pembelian"));
    }

}