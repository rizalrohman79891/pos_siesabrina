<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mutasi_barang extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('mutasi_kas_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->loginCheck();
    }

    public function index()
    {
        $data_barang = $this->ModelUniv->read("barang");

		$this->db->select("*");
		$this->db->from("mutasi_barang");
		$this->db->join("barang", "barang.id_barang = mutasi_barang.id_barang");
		$this->db->order_by("mutasi_barang.created_at", 'DESC');

		$data_mutasi_barang = $this->db->get()->result();

		$this->load->view("superadmin/view-mutasi-barang", [
			'data_mutasi_barang' => $data_mutasi_barang,
			'data_barang' => $data_barang,
		]);
    }

	public function tambah_data_mutasi_barang()
	{
		$data = [
			'id_barang' => $this->input->post("id_barang"),
			'tanggal' => date('Y-m-d H:i').':'.date('s'),
			'stok' => $this->input->post("stok"),   
			'jenis' => $this->input->post("jenis"),   
			'keterangan' => $this->input->post("keterangan"),  
		];

		$this->ModelUniv->create($data, 'mutasi_barang');

		$this->db->select('*');
		$this->db->from('barang');
		$this->db->where('id_barang', $data['id_barang']);

		$barang = $this->db->get()->row();

		if ($data['jenis'] == 'Toko ke Gudang') {
			$update_toko = [
				'stok_toko' => $barang->stok_toko -= $data['stok'],
				'stok_gudang' => $barang->stok_gudang += $data['stok']
			];
			$this->ModelUniv->update([
                'id_barang' => $data['id_barang']
            ], 'barang', $update_toko);
		} else {
			$update_gudang = [
				'stok_toko' => $barang->stok_toko += $data['stok'],
				'stok_gudang' => $barang->stok_gudang -= $data['stok']
			];
			$this->ModelUniv->update([
                'id_barang' => $data['id_barang']
            ], 'barang', $update_gudang);
		}

		return redirect(base_url("mutasi_barang"));
	}

}