<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hutang extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('string');
        $this->loginCheck();
    }

    public function bayar_hutang()
    {
        // Total hutang di tabel hutang berkurang sesuai dengan jumlah pembayaran
        $total_hutang = $this->input->post("sisa_hutang");
        $total_bayar = $this->input->post("total_bayar");
        $sisa_hutang = $total_hutang - $total_bayar;


        $data_update_total_hutang = [
            'total_hutang' => $sisa_hutang
        ];

        $this->ModelUniv->update(['id_hutang' => $this->input->post("id_hutang")], 'hutang', $data_update_total_hutang);

        // Catat di riwayat bayar hutang
        $data_bayar_hutang = [
            'id_hutang' => $this->input->post('id_hutang'),
            'jenis_pembayaran' => $this->input->post('jenis_pembayaran'),
            'total_bayar' => $this->input->post('total_bayar'),
        ];

        $this->ModelUniv->create($data_bayar_hutang, 'bayar_hutang');
        return redirect(base_url("laporan/laporan_hutang"));
    }

}