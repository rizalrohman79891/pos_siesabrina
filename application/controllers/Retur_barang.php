<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur_barang extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_model');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('string');
        $this->loginCheck();
    }

    public function index()
    {
		$this->load->view("superadmin/view-retur-barang");
    }
}