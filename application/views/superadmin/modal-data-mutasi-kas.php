<!-- Modal -->
<div class="modal fade" id="modalTambahMutasiKas" tabindex="-1" role="dialog" aria-labelledby="modalTambahMutasiKasLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Mutasi Kas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("mutasi_kas/tambah_data_mutasi_kas") ?>">

          <div class="form-group">
            <label>Tanggal</label>
            <input type="datetime-local" name="tanggal" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Jumlah</label>
            <input type="number" name="jumlah" placeholder="Jumlah" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Jenis</label>
            <select class="form-control" name="jenis" required="">
              <option>Pemasukkan</option>
              <option>Pengeluaran</option>
            </select>
          </div>

          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" id="keterangan" required="" class="form-control"></textarea>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Barang -->
<?php foreach ($data_mutasi_kas as $row) : ?>
<div class="modal fade" id="modalEditMutasiKas<?php echo $row->id_mutasi_kas ?>" tabindex="-1" role="dialog" aria-labelledby="modalEditMutasiKasLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Mutasi Kas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?= base_url("mutasi_kas/edit_data_mutasi_kas") ?>">

          <input type="hidden" name="id_mutasi_kas" value="<?php echo $row->id_mutasi_kas ?>">

          <div class="form-group">
            <label>Tanggal</label>
            <input type="datetime-local" name="tanggal" required="" class="form-control" value="<?= date('Y-m-d\TH:i', strtotime($row->tanggal)) ?>">
          </div>

          <div class="form-group">
            <label>Jumlah</label>
            <input type="number" name="jumlah" placeholder="Jumlah" required="" class="form-control" value="<?php echo $row->jumlah ?>">
          </div>

          <div class="form-group">
            <label>Jenis</label>
            <select class="form-control" name="jenis" required="" value="<?php echo $row->jenis ?>">
              <option value="Pemasukkan">Pemasukkan</option>
              <option value="Pengeluaran">Pengeluaran</option>
            </select>
          </div>

          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" id="keterangan" required="" class="form-control"><?= $row->keterangan ?></textarea>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>