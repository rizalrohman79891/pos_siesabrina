<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Barang</h1>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalTambahBarang">
              Tambah Data Barang
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Stok</th>
                            <th>Harga</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_barang as $row) : ?>
                        <tr>
                            <td><?php echo $no ?></td>
                            <td><?php echo $row->nama_barang ?></td>
                            <td>
                                <button class="btn btn-secondary btn-default btn-sm">Toko : <?php echo $row->stok_toko; ?></button>
                                <button class="btn btn-secondary btn-default btn-sm">Gudang : <?php echo $row->stok_gudang; ?></button>
                            </td>
                            <td><button class="btn btn-secondary btn-default btn-sm">Umum : <?php echo $row->harga_jual_umum; ?></button>
                                <button class="btn btn-secondary btn-default btn-sm">Grosir : <?php echo $row->harga_jual_grosir; ?></button>
                                <button class="btn btn-secondary btn-default btn-sm">Khusus : <?php echo $row->harga_jual_khusus; ?></button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modalEditBarang<?php echo $row->id_barang ?>">
                                  <i class="fa fa-edit"></i>
                                </button>
                                <a href="<?php echo base_url("superadmin/delete_data_barang/").$row->id_barang ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                <a href="<?php echo base_url("superadmin/cetak_barcode/").$row->barcode ?>" class="btn btn-primary btn-sm"><i class="fa fa-barcode"></i></a>
                            </td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php include 'modal-data-barang.php' ?>
<?php include 'partials/view-footer.php' ?>