<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-3 text-gray-800">Data Penjualan</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <h5 class="card-title">Form Penjualan</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="<?= base_url() ?>penjualan/process" id="formTransaksi" method="POST">
                        <div class="col-md-12">
                            <div class="row justify-content-between">
                                <div class="input-group col-md-6 row">
                                    <label class="control-label col-sm-2 col-md-2 col-xs-12" for="">Barcode : </label>
                                    <div class="col-sm-6 col-md-6 col-xs-12">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" id="barcode" readonly placeholder="" aria-label="" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" data-toggle="modal" data-target="#modalTransaksiBarang" type="button"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 row">
                                    <div class="col-sm-12 col-md-12 col-xs-12 text-right">
                                        <input type="text" class="border-0" name="no_faktur" readonly value="<?= $no_faktur ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 row">
                                    <label class="control-label col-sm-2 col-md-2 col-xs-12" for="">Barang : </label>
                                    <div class="col-sm-4 col-md-4 col-xs-12">
                                        <input type="text" id="nama_barang" readonly class="form-control">
                                        <input type="hidden" id="id_barang">
                                        <input type="hidden" id="stok_hidden">
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-xs-12 pt-2">
                                        <span id="harga_barang"></span>
                                        <input type="hidden" id="harga_barang_hidden">
                                    </div>
                                </div>
                                <div class="form-group mb-0 col-md-6 text-right">
                                    <h1 style="font-weight: 600;">Rp. <span id="totalSeluruh">0</span></h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 row">
                                    <label class="control-label col-sm-2 col-md-2 col-xs-12" for="">Quantity : </label>
                                    <div class="col-sm-4 col-md-4 col-xs-12">
                                        <input type="number" class="form-control" id="quantity">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-2 mb-3">
                            <button type="button" class="btn btn-primary" id="tambahKeCart"><i class="fa fa-plus-circle"></i> Tambah</button>
                            <button type="button" class="btn btn-success" id="btnModalBayar" data-target="#modalBayarPenjualan" data-toggle="modal"><i class="fa fa-save"></i> Bayar</button>
                            <a href="" class="btn btn-info"><i class="fa fa-file"></i> Transaksi Baru</a>
                        </div>
                        <div class="col-md-12">
                            <table id="tableBarang" width="100%" cellspacing="0" class="table table-striped table-bordered tableBarang">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Barang</th>
                                        <th>Harga Barang</th>
                                        <th>QTY</th>
                                        <th>Diskon</th>
                                        <th>Subtotal</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody id="parentTambahBarang">
                                    <tr align="center">
                                        <td colspan="8">Tidak ada item produk!</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="showInformasiHarga">
                                <div class="row" style="margin-bottom: 0.5em;">
                                    <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                        <input type="hidden" id="totalHargaVal" name="total">
                                        <input type="hidden" id="totalDiskonVal" name="total_diskon">
                                        <input type="hidden" id="kembalianTotal" name="kembalian">
                                        <input type="hidden" id="inputBayarHidden" name="total_bayar">
                                        <input type="hidden" id="jenisBayarHidden" name="jenis_bayar">
                                        <input type="hidden" id="keteranganHidden" name="keterangan">
                                        <input type="hidden" id="idRekeningHidden" name="id_rekening">
                                        <input type="hidden" id="idPelangganHidden" name="id_pelanggan">
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="d-none" id="simpanTransaksi">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include 'modal-penjualan.php' ?>
<?php include 'partials/view-footer.php' ?>
<script>
    $(function () {

        function generateCurrency(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }

        function format1(n, currency) {
            return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
            });
        }

        let totalHarga = 0

        $(".modalBarang").attr('disabled', String(true))
        $(".showInformasiHarga").hide()
        $("#btnModalBayar").attr('disabled', true)
        $("#tambahKeCart").attr('disabled', true)

        $(document).on('change', '#id_supplier', function () {
            if($(this).val() > 0) {
                $(".modalBarang").removeAttr('disabled', String(true))
            } else {
                $(".modalBarang").attr('disabled', String(true))
            } 
        })

        $(document).on('click', '.selectBarang', function (e) {
            let id = $(this).data('id')
            let nama = $(this).data('nama')
            let stok = $(this).data('stok')
            let harga = $(this).data('harga')
            let barcode = $(this).data('barcode')

            if(stok == 0) {
                Swal.fire('Stok Habis!', '', 'info')
            } else {

                $('#modalTransaksiBarang').modal('hide');
                let check = $('.id_barang:contains('+id+')').length

                if(check == 1) {
                    Swal.fire('Barang sudah ada dalam list!', '', 'info')
                    $('#modalTransaksiBarang').modal('hide');
                } else {
                    $("#id_barang").val(id)
                    $("#nama_barang").val(nama)
                    $("#harga_barang").text(format1(harga, 'Rp. '))
                    $("#harga_barang_hidden").val(harga)
                    $("#barcode").val(barcode)
                    $("#quantity").val(1)
                    $("#stok_hidden").val(stok)
    
                    $("#quantity").focus()
                    $("#quantity").attr('max', stok)
                }
            }
            $("#tambahKeCart").removeAttr('disabled', true)
        })

        $(document).on('click', '#tambahKeCart', function () {

            let id = $("#id_barang").val()
            let nama = $("#nama_barang").val()
            let harga = $("#harga_barang_hidden").val()
            let quantity = $("#quantity").val()
            let stok_hidden = $("#stok_hidden").val()

            console.log(quantity)

            let appendData = ''
    
            let tbody = $('#tableBarang tbody tr td').text();

            appendData += `<tr>
                <td>#<span class="id_barang" hidden>${id}</span></td>
                <td>${nama}</td>
                <input type="hidden" name="id_barang[]" value="${id}">
                <td>${format1(parseInt(harga), 'Rp. ')}</td>
                <td style="width: 150px;">
                    <input type="hidden" name="harga_jual[]" class="hargaJual" value="${harga}">
                    <input type="number" name="jumlah[]" min="1" max="${stok_hidden}" class="form-control input-sm qty" value="${quantity}">
                </td>
                <td style="width: 150px;">
                    <input type="number" value="0" class="form-control input-sm diskon">
                </td>
                <input type="hidden" class="subTotalHidden" name="subtotal[]" value="${harga * quantity}">
                <input type="hidden" class="diskonHidden" name="diskon[]" value="0">
                <td><input type="text" value="${format1(parseInt(harga * quantity), 'Rp. ')}" readonly class="form-control input-sm subTotal"></td>
                <td><button type="button" class="btn btn-danger btnRemoveBarang"><i class="fa fa-trash"></i></button></td>
            </tr>`;

            if(tbody == 'Tidak ada item produk!' || tbody == '')

            $('#tableBarang tbody tr').remove();
            $('#tableBarang tbody').append(appendData);
            totalHarga = totalHarga + ( parseFloat(harga) * parseInt(quantity) );

            $('#totalHarga').val(format1(totalHarga, 'Rp. '));
            $('#totalBayar').val(format1(totalHarga, ''))
            $('#grandTotal').text(format1(totalHarga, ''))
            $('#totalSeluruh').text(format1(totalHarga, ''))
            $("#btnModalBayar").removeAttr('disabled', true)

            $('#totalHargaVal').val(totalHarga);
            $(".showInformasiHarga").show()

            $("#barcode").val('')
            $("#id_barang").val('')
            $("#nama_barang").val('')
            $("#quantity").val('')
            $("#harga_barang").text('')
            $("#harga_barang_hidden").val('')
            $("#stok_hidden").val('')

            $("#tambahKeCart").attr('disabled', true)
        })

        function calcSubTotal(param) {
            let qty = parseInt($(param).closest('tr').find('.qty').val());
            let hargaJual = parseFloat($(param).closest('tr').find('.hargaJual').val());
            let subTotalAwal = parseFloat($(param).closest('tr').find('.subTotalHidden').val());
            let subTotal = qty * hargaJual;
            totalHarga += subTotal - subTotalAwal;
            $(param).closest('tr').find('.subTotalHidden').val(subTotal);
            $(param).closest('tr').find('.subTotal').val(format1(subTotal, 'Rp. '));
            $('#totalHarga').val(format1(totalHarga, 'Rp. '));
            $('#totalHargaVal').val(totalHarga);

            $('#totalBayar').val(format1(totalHarga, ''))
            $('#grandTotal').text(format1(totalHarga, ''))
            $('#totalSeluruh').text(format1(totalHarga, ''))
        }

        function attributeDisabledQTY(param) {
            let minValue = parseInt($(param).attr('min'));
            let maxValue = parseInt($(param).attr('max'));
            let currentValue = parseInt($(param).val());
            
            if (currentValue >= minValue) {
                $('.btn-number[data-type="minus"]').removeAttr('disabled');
            } 
            if (currentValue <= maxValue) {
                $('.btn-number[data-type="plus"]').removeAttr('disabled');                        
            } 
        }

        function qtyChange(param) {
            if (isNaN($(param).val()) || $(param).val() < 1) {
                $(param).val('1');
            } else {
                calcSubTotal(param);
                attributeDisabledQTY(param);
            }
        }

        $(document).on('keyup', '.diskon', function () {
            $(this).val(generateCurrency($(this).val(), ''))

            if($(this).val() == '') $(this).val(0)

            let column = $(this).closest('tr')
            let harga = column.find('td').find('.hargaJual').val()
            let quantity = column.find('td').find('.qty').val()
            // console.log(quantity)
            let diskon = parseInt($(this).val().split('.').join(''))

            let result = (harga * quantity) - diskon

            column.find('.diskonHidden').val(diskon)
            column.find('.subTotalHidden').val(result > 0 ? result : 0)
            column.find('td').find('.subTotal').val(format1(result > 0 ? result : 0, 'Rp. '))

            hitungKeseluruhan()
        })

        function hitungKeseluruhan() {
            let totalSeluruh = 0
            let totalDiskon = 0
            $('input[name="subtotal[]"]').each(function() {
                totalSeluruh += parseInt($(this).val())
            })

            $('input[name="diskon[]"]').each(function() {
                totalDiskon += parseInt($(this).val())
            })
            
            $('#totalHarga').val(format1(totalSeluruh, 'Rp. '));
            $('#totalHargaVal').val(totalSeluruh);

            $('#totalBayar').val(format1(totalSeluruh, ''))
            $('#grandTotal').text(format1(totalSeluruh, ''))
            $('#totalSeluruh').text(format1(totalSeluruh, ''))
            $("#totalDiskonVal").val(totalDiskon)
            $("#totalDiskon").val(format1(totalDiskon, ''))
        }

        $(document).on('keyup', '.qty', function () {
            if($(this).val() > parseInt($(this).attr('max'))) {
                $(this).val(parseInt($(this).attr('max')))
            }
            qtyChange(this)
        })

        $(document).on('change', '.qty', function () {
            if($(this).val() > parseInt($(this).attr('max'))) {
                $(this).val(parseInt($(this).attr('max')))
            }
            qtyChange(this)
        })

        $(document).on('click', '.btnRemoveBarang', function () {
            let tbody = $('#tableBarang tbody tr').length;
            let subTotalAwal = parseFloat($(this).closest('tr').find('.subTotalHidden').val());
            totalHarga -= subTotalAwal;
            $(this).closest('tr').remove();
            $('#totalHarga').val(totalHarga);         
            $('#totalHargaVal').val(totalHarga);

            $('#totalBayar').val(format1(totalHarga, ''))
            $('#grandTotal').text(format1(totalHarga, ''))

            if(tbody == 1) $(".showInformasiHarga").hide() 
            $("#btnModalBayar").attr('disabled', true)
            $("#tambahKeCart").attr('disabled', true)

        });

        $(document).on('keyup', '#inputBayar', function () {
            $(this).val(generateCurrency($(this).val(), ''))
            $('#inputBayarHidden').val($(this).val().split('.').join(''))

            let inputBayar = $('#inputBayarHidden').val()
            let totalBayar = $("#totalHargaVal").val()
            let kembalian = inputBayar - totalBayar

            $("#kembalian").text(format1(kembalian, ''))
            $("#kembalianDiModal").val(kembalian)
            $("#kembalianTotal").val(kembalian)
        })

        $(document).on('click', '#bayarTransaksi', function () {
            // $("#modalBayarTransaksi").modal('hide')
            // console.log($('#formTransaksi').serializeArray())
            $("#formTransaksi").submit()
        })

        $("#jenisBayarHidden").val($('#jenis_bayar').val())

        $('.transfer').hide()

        $(document).on('change', '#jenis_bayar', function () {
            $("#jenisBayarHidden").val($(this).val())

            let jenis = ["TRANSFER"]
            if(jenis.includes($(this).val())) {
                $('.transfer').show()
                $('.bayar').hide()
            } else {
                $('.transfer').hide()
                $('.bayar').show()
            }
        })

        $("#keteranganHidden").val($('#keterangan').val())

        $(document).on('keyup', '#keterangan', function () {
            $("#keteranganHidden").val($(this).val())
        })

        $("#idRekeningHidden").val($("#id_rekening").val())

        $(document).on('change', '#id_rekening', function () {
            $("#idRekeningHidden").val($(this).val())
        })

        $("#idPelangganHidden").val($("#id_pelanggan"))

        $(document).on('change', '#id_pelanggan', function () {
            $("#idPelangganHidden").val($(this).val())
        })
    })
</script>