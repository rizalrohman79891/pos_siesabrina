<div class="modal fade" id="modalTambahSupplier" tabindex="-1" role="dialog" aria-labelledby="modalTambahPelangganLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("superadmin/tambah_data_supplier") ?>">
          <div class="form-group">
            <label>Nama Supplier</label>
            <input type="text" name="nama_supplier" required="" class="form-control" placeholder="Masukan Nama Pelanggan">
          </div>
          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="no_tlp" required="" class="form-control" placeholder="Masukan Nomor Telepon">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat" rows="10" placeholder="Masukan Alamat Pelanggan" required=""></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit -->
<?php foreach ($data_supplier as $row) : ?>
<div class="modal fade" id="modalEditSupplier<?php echo $row->id_supplier ?>" tabindex="-1" role="dialog" aria-labelledby="modalTambahPelangganLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("superadmin/edit_data_supplier") ?>">

          <div class="form-group">
            <label>Nama pelanggan</label>
            <input type="hidden" name="id_supplier" value="<?php echo $row->id_supplier ?>">
            <input type="text" name="nama_supplier" required="" class="form-control" placeholder="Masukan Nama supplier" value="<?php echo $row->nama_supplier?>">
          </div>
          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="no_tlp" required="" class="form-control" placeholder="Masukan Nomor Telepon" value="<?php echo $row->no_tlp ?>">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat" rows="10" placeholder="Masukan Alamat supplier" required=""><?php echo $row->alamat; ?></textarea>
          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>