<h2><center>LAPORAN STOK KELUAR</center></h2>
<div class="table-responsive text-center">
    <table border="1" width="100%" style="text-align:center;">
        <thead>
            <tr>
                <th>#</th>
                <th>Tanggal</th>
                <th>Nama Barang</th>
                <th>Jumlah</th>
                <!-- <th>Action</th> -->
            </tr>
        </thead>
        <tbody>
            <?php $no = 1; ?>
            <?php foreach ($data_penjualan as $row) : ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $row->created_at; ?></td>
                <td><?php echo $row->nama_barang; ?></td>
                <td><?php echo $row->quantity; ?></td>
            </tr>
            <?php $no++ ?>
            <?php endforeach; ?>
        </tbody>

    </table>
</div>
