<!-- Modal -->
<div class="modal fade" id="modalTambahBarang" tabindex="-1" role="dialog" aria-labelledby="modalTambahBarangLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("superadmin/tambah_data_barang") ?>">

          <div class="form-group">
            <label>Barcode</label>
            <input type="text" name="barcode" placeholder="Scan Code Barcode" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Nama Barang</label>
            <input type="text" name="nama_barang" placeholder="Masukan Nama Barang" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Kategori</label>
            <select class="form-control" name="kategori" required="">
              <option>Atasan</option>
              <option>Bawahan</option>
              <option>Celana</option>
              <option>Baju</option>
            </select>
          </div>

          <div class="form-group">
            <label>Harga Beli</label>
            <input type="number" name="harga_beli" placeholder="Masukan Harga Beli" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Harga Jual Umum</label>
            <input type="number" name="harga_jual_umum" placeholder="Masukan Harga Jual Umum" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Harga Jual Grosir</label>
            <input type="number" name="harga_jual_grosir" placeholder="Masukan Harga Jual Grosir" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Harga Jual Khusus</label>
            <input type="number" name="harga_jual_khusus" placeholder="Masukan Harga Jual Khusus" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Stok Toko</label>
            <input type="number" name="stok_toko" placeholder="Masukan Stok Toko" required="" class="form-control">
          </div>

          <div class="form-group">
            <label>Stok Gudang</label>
            <input type="number" name="stok_gudang" placeholder="Masukan Stok Gudang" required="" class="form-control">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Barang -->
<?php foreach ($data_barang as $row) : ?>
<div class="modal fade" id="modalEditBarang<?php echo $row->id_barang ?>" tabindex="-1" role="dialog" aria-labelledby="modalTambahBarangLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("superadmin/edit_data_barang") ?>">

          <div class="form-group">
            <label>Barcode</label>
            <input type="hidden" name="id_barang" value="<?php echo $row->id_barang ?>">
            <input type="text" name="barcode" placeholder="Scan Code Barcode" required="" class="form-control" value="<?php echo $row->barcode ?>">
          </div>

          <div class="form-group">
            <label>Nama Barang</label>
            <input type="text" name="nama_barang" placeholder="Masukan Nama Barang" required="" class="form-control" value="<?php echo $row->nama_barang ?>">
          </div>

          <div class="form-group">
            <label>Harga Beli</label>
            <input type="number" name="harga_beli" placeholder="Masukan Harga Beli" required="" class="form-control" value="<?php echo $row->harga_beli ?>">
          </div>

          <div class="form-group">
            <label>Harga Jual Umum</label>
            <input type="number" name="harga_jual_umum" placeholder="Masukan Harga Jual Umum" required="" class="form-control" value="<?php echo $row->harga_jual_umum ?>">
          </div>

          <div class="form-group">
            <label>Harga Jual Grosir</label>
            <input type="number" name="harga_jual_grosir" placeholder="Masukan Harga Jual Grosir" required="" class="form-control" value="<?php echo $row->harga_jual_grosir ?>">
          </div>

          <div class="form-group">
            <label>Harga Jual Khusus</label>
            <input type="number" name="harga_jual_khusus" placeholder="Masukan Harga Jual Khusus" required="" class="form-control" value="<?php echo $row->harga_jual_khusus ?>">
          </div>

          <div class="form-group">
            <label>Stok Toko</label>
            <input type="number" name="stok_toko" placeholder="Masukan Stok Toko" required="" class="form-control" value="<?php echo $row->stok_toko ?>" readonly="">
          </div>

          <div class="form-group">
            <label>Stok Gudang</label>
            <input type="number" name="stok_gudang" placeholder="Masukan Stok Gudang" required="" class="form-control" value="<?php echo $row->stok_gudang ?>" readonly="">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>