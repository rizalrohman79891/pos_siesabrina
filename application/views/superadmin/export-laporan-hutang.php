<h2><center>LAPORAN HUTANG</center></h2>
<table border="1" width="100%" style="text-align:center;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Nama Pelanggan</th>
                            <th>Jumlah Hutang</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_hutang as $row) : ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->created_at; ?></td>
                            <td><?php echo $row->nama_pelanggan; ?></td>
                            <td>Rp.<?php echo number_format($row->total_hutang); ?></td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>

                </table>