<div class="modal fade" id="modalTambahPelanggan" tabindex="-1" role="dialog" aria-labelledby="modalTambahPelangganLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pelanggan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("superadmin/tambah_data_pelanggan") ?>">
          <div class="form-group">
            <label>Nama pelanggan</label>
            <input type="text" name="nama_pelanggan" required="" class="form-control" placeholder="Masukan Nama Pelanggan">
          </div>
          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="no_tlp" required="" class="form-control" placeholder="Masukan Nomor Telepon">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat" rows="10" placeholder="Masukan Alamat Pelanggan" required=""></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Barang -->
<?php foreach ($data_pelanggan as $row) : ?>
<div class="modal fade" id="modalEditPelanggan<?php echo $row->id_pelanggan ?>" tabindex="-1" role="dialog" aria-labelledby="modalTambahPelangganLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Pelanggan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("superadmin/edit_data_pelanggan") ?>">

          <div class="form-group">
            <label>Nama pelanggan</label>
            <input type="hidden" name="id_pelanggan" value="<?php echo $row->id_pelanggan ?>">
            <input type="text" name="nama_pelanggan" required="" class="form-control" placeholder="Masukan Nama Pelanggan" value="<?php echo $row->nama_pelanggan?>">
          </div>
          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="no_tlp" required="" class="form-control" placeholder="Masukan Nomor Telepon" value="<?php echo $row->no_tlp ?>">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat" rows="10" placeholder="Masukan Alamat Pelanggan" required=""><?php echo $row->alamat; ?></textarea>
          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>