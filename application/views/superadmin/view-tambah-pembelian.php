<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-3 text-gray-800">Data Pembelian</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <h5 class="card-title">Form Pembelian</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="<?= base_url() ?>pembelian/process" id="formTransaksi" method="POST">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6 row">
                                    <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Kode Pembelian : </label>
                                    <div class="col-sm-8 col-md-8 col-xs-12">
                                        <input type="text" class="form-control" name="no_faktur" readonly value="<?= $no_faktur ?>">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 row">
                                    <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Tanggal Pembelian : </label>
                                    <div class="col-sm-8 col-md-8 col-xs-12">
                                        <input type="datetime-local" class="form-control" name="tanggal" value="<?= date('Y-m-d\TH:i') ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 row">
                                    <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Pilih Supplier : </label>
                                    <div class="col-sm-8 col-md-8 col-xs-12">
                                        <select name="id_supplier" id="id_supplier" required="" class="form-control">
                                            <option value="">--- Pilih ---</option>
                                            <?php $no = 1; ?>
                                            <?php foreach ($data_supplier as $key => $value) : ?>
                                                <option value="<?= $value->id_supplier ?>"><?= $no++ ?>.<?= ' '.$value->nama_supplier ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 row">
                                    <div class="col-sm-8 col-md-8 col-xs-12">
                                        <button class="btn btn-primary modalBarang" type="button" data-toggle="modal" data-target="#modalTransaksiBarang"><i class="fa fa-plus-circle"></i> Tambah Barang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table id="tableBarang" width="100%" cellspacing="0" class="table table-striped table-bordered tableBarang">
                                <thead>
                                    <tr>
                                        <th>Nama Barang</th>
                                        <th>Harga Barang</th>
                                        <th>QTY</th>
                                        <th>Subtotal</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody id="parentTambahBarang"></tbody>
                            </table>
                            <div class="showInformasiHarga">
                                <div class="row" style="margin-bottom: 0.5em;">
                                    <div class="col-md-10 col-sm-12 col-xs-12 form-group text-right">
                                        <label for="" class="control-label col-md-12 col-sm-3 col-xs-12">Total Harga : <b>Rp. </b></label>
                                    </div>
                                    <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                        <input type="text" id="totalHarga" readonly value="0" class="form-control">
                                        <input type="hidden" id="totalHargaVal" name="total">
                                        <input type="hidden" id="kembalianTotal" name="kembalian">
                                        <input type="hidden" id="inputBayarHidden" name="total_bayar">
                                        <input type="hidden" id="jenisBayarHidden" name="jenis_bayar">
                                        <input type="hidden" id="keteranganHidden" name="keterangan">
                                        <input type="hidden" id="idRekeningHidden" name="id_rekening">
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="d-none" id="simpanTransaksi">Simpan</button>
                                    <button type="button" class="btn btn-success" data-target="#modalBayarTransaksi" data-toggle="modal"><i class="fa fa-chevron-right"></i> Lanjut</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include 'modal-transaksi-barang.php' ?>
<?php include 'partials/view-footer.php' ?>
<script>
    $(function () {

        function generateCurrency(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }

        function format1(n, currency) {
            return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
            });
        }

        let totalHarga = 0

        $(".modalBarang").attr('disabled', String(true))
        $(".showInformasiHarga").hide()

        $(document).on('change', '#id_supplier', function () {
            if($(this).val() > 0) {
                $(".modalBarang").removeAttr('disabled', String(true))
            } else {
                $(".modalBarang").attr('disabled', String(true))
            } 
        })

        $(document).on('click', '.selectBarang', function (e) {
            let id = $(this).data('id')
            let nama = $(this).data('nama')
            let stok = $(this).data('stok')
            let harga = $(this).data('harga')

            if(stok == 0) {
                let appendData = ''
    
                let tbody = $('#tableBarang tbody tr td').text();
    
                appendData += `<tr>
                    <td>${nama}</td>
                    <input type="hidden" name="id_barang[]" value="${id}">
                    <input type="hidden" name="harga_jual[]" class="hargaJual" value="${harga}">
                    <td>${format1(harga, 'Rp. ')}</td>
                    <td style="width: 150px;">
                        <input type="number" name="jumlah[]" value="1" min="1" max="${stok}" class="form-control input-sm qty">
                    </td>
                    <input type="hidden" class="subTotalHidden" name="subtotal[]" value="${harga}">
                    <td><input type="text" value="${format1(harga, 'Rp. ')}" readonly class="form-control input-sm subTotal"></td>
                    <td><button type="button" class="btn btn-danger btnRemoveBarang"><i class="fa fa-trash"></i></button></td>
                </tr>`;
    
                if(tbody == 'Belum Ada Data!' || tbody == '')
    
                $('#tableBarang tbody tr').remove();
                $('#tableBarang tbody').append(appendData);
                totalHarga = totalHarga + parseFloat(harga);
    
                $('#totalHarga').val(format1(totalHarga, 'Rp. '));
                $('#totalBayar').val(format1(totalHarga, ''))
                $('#grandTotal').text(format1(totalHarga, ''))
    
                $('#totalHargaVal').val(totalHarga);
                $('#modalTransaksiBarang').modal('hide');
                $(".showInformasiHarga").show()
            } else {
                let appendData = ''
    
                let tbody = $('#tableBarang tbody tr td').text();
    
                appendData += `<tr>
                    <td>${nama}</td>
                    <input type="hidden" name="id_barang[]" value="${id}">
                    <input type="hidden" name="harga_jual[]" class="hargaJual" value="${harga}">
                    <td>${format1(harga, 'Rp. ')}</td>
                    <td style="width: 150px;">
                        <input type="number" name="jumlah[]" value="1" min="1" max="${stok}" class="form-control input-sm qty">
                    </td>
                    <input type="hidden" class="subTotalHidden" name="subtotal[]" value="${harga}">
                    <td><input type="text" value="${format1(harga, 'Rp. ')}" readonly class="form-control input-sm subTotal"></td>
                    <td><button type="button" class="btn btn-danger btnRemoveBarang"><i class="fa fa-trash"></i></button></td>
                </tr>`;
    
                if(tbody == 'Belum Ada Data!' || tbody == '')
    
                $('#tableBarang tbody tr').remove();
                $('#tableBarang tbody').append(appendData);
                totalHarga = totalHarga + parseFloat(harga);
    
                $('#totalHarga').val(format1(totalHarga, 'Rp. '));
                $('#totalBayar').val(format1(totalHarga, ''))
                $('#grandTotal').text(format1(totalHarga, ''))
    
                $('#totalHargaVal').val(totalHarga);
                $('#modalTransaksiBarang').modal('hide');
                $(".showInformasiHarga").show()
            }

        })

        function calcSubTotal(param) {
            let qty = parseInt($(param).closest('tr').find('.qty').val());
            let hargaJual = parseFloat($(param).closest('tr').find('.hargaJual').val());
            let subTotalAwal = parseFloat($(param).closest('tr').find('.subTotalHidden').val());
            let subTotal = qty * hargaJual;
            totalHarga += subTotal - subTotalAwal;
            $(param).closest('tr').find('.subTotalHidden').val(subTotal);
            $(param).closest('tr').find('.subTotal').val(format1(subTotal, 'Rp. '));
            $('#totalHarga').val(format1(totalHarga, 'Rp. '));
            $('#totalHargaVal').val(totalHarga);

            $('#totalBayar').val(format1(totalHarga, ''))
            $('#grandTotal').text(format1(totalHarga, ''))
        }

        function attributeDisabledQTY(param) {
            let minValue = parseInt($(param).attr('min'));
            let maxValue = parseInt($(param).attr('max'));
            let currentValue = parseInt($(param).val());
            
            if (currentValue >= minValue) {
                $('.btn-number[data-type="minus"]').removeAttr('disabled');
            } 
            if (currentValue <= maxValue) {
                $('.btn-number[data-type="plus"]').removeAttr('disabled');                        
            } 
        }

        function qtyChange(param) {
            if (isNaN($(param).val()) || $(param).val() < 1) {
                $(param).val('1');
            } else {
                calcSubTotal(param);
                attributeDisabledQTY(param);
            }
        }

        $(document).on('keyup', '.qty', function () {
            qtyChange(this)
        })

        $(document).on('change', '.qty', function () {
            qtyChange(this)
        })

        $(document).on('click', '.btnRemoveBarang', function () {
            let tbody = $('#tableBarang tbody tr').length;
            let subTotalAwal = parseFloat($(this).closest('tr').find('.subTotalHidden').val());
            totalHarga -= subTotalAwal;
            $(this).closest('tr').remove();
            $('#totalHarga').val(totalHarga);         
            $('#totalHargaVal').val(totalHarga);

            $('#totalBayar').val(format1(totalHarga, ''))
            $('#grandTotal').text(format1(totalHarga, ''))

            if(tbody == 1) $(".showInformasiHarga").hide()

        });

        $(document).on('keyup', '#inputBayar', function () {
            $(this).val(generateCurrency($(this).val(), ''))
            $('#inputBayarHidden').val($(this).val().split('.').join(''))

            let inputBayar = $('#inputBayarHidden').val()
            let totalBayar = $("#totalHargaVal").val()
            let kembalian = inputBayar - totalBayar

            $("#kembalian").text(format1(kembalian, ''))
            $("#kembalianTotal").val(kembalian)
        })

        $(document).on('click', '#bayarTransaksi', function () {
            // $("#modalBayarTransaksi").modal('hide')
            // console.log($('#formTransaksi').serializeArray())
            $("#formTransaksi").submit()
        })

        $("#jenisBayarHidden").val($('#jenis_bayar').val())

        $('.transfer').hide()

        $(document).on('change', '#jenis_bayar', function () {
            $("#jenisBayarHidden").val($(this).val())

            let jenis = ["TRANSFER"]
            if(jenis.includes($(this).val())) {
                $('.transfer').show()
                $('.bayar').hide()
            } else {
                $('.transfer').hide()
                $('.bayar').show()
            }
        })

        $("#keteranganHidden").val($('#keterangan').val())

        $(document).on('keyup', '#keterangan', function () {
            $("#keteranganHidden").val($(this).val())
        })

        $("#idRekeningHidden").val($("#id_rekening").val())

        $(document).on('change', '#id_rekening', function () {
            $("#idRekeningHidden").val($(this).val())
        })
    })
</script>