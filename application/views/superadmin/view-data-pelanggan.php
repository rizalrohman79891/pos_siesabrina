<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Pelanggan</h1>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalTambahPelanggan">
              Tambah Data Pelanggan
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Pelanggan</th>
                            <th>Telepon</th>
                            <th>Alamat</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_pelanggan as $row) : ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->nama_pelanggan; ?></td>
                            <td><?php echo $row->no_tlp; ?></td>
                            <td><?php echo $row->alamat; ?></td>
                            <td>
                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modalEditPelanggan<?php echo $row->id_pelanggan ?>">
                                  <i class="fa fa-edit"></i>
                                </button>
                                <a class="btn btn-success btn-sm" href="wa.me/<?php echo $row->no_tlp ?>"><i class="fab fa-whatsapp"></i></a>
                                <a class="btn btn-danger btn-sm" href="<?php echo base_url("superadmin/delete_data_pelanggan/").$row->id_pelanggan ?>"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>

</div>
<?php include 'modal-data-pelanggan.php' ?>
<?php include 'partials/view-footer.php' ?>