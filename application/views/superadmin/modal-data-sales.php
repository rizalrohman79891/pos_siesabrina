<div class="modal fade" id="modalTambahSales" tabindex="-1" role="dialog" aria-labelledby="modalTambahSalesLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Sales</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("superadmin/tambah_data_sales") ?>">
          <div class="form-group">
            <label>Nama Sales</label>
            <input type="text" name="nama_sales" required="" class="form-control" placeholder="Masukan Nama Sales">
          </div>
          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="no_tlp" required="" class="form-control" placeholder="Masukan Nomor Telepon">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat" rows="10" placeholder="Masukan Alamat sales" required=""></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
