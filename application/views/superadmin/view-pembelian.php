<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Pembelian</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <a href="<?= base_url() ?>pembelian/create" class="btn btn-primary btn-sm">
              <i class="fa fa-plus-circle"></i> Tambah Pembelian
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No Faktur</th>
                            <th>Supplier</th>
                            <th>Tanggal</th>
                            <th>Metode Bayar</th>
                            <th>Total</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_pembelian as $key => $value) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $value->no_faktur ?></td>
                                <td><?= $value->nama_supplier ?></td>
                                <td><?= $value->tanggal ?></td>
                                <td>
                                    <?php
                                        $badge = '';
                                        if ($value->jenis_bayar == 'TRANSFER') {
                                            $badge = 'primary';
                                        } else if ($value->jenis_bayar == 'CREDIT') {
                                            $badge = 'warning';
                                        } else {
                                            $badge = 'info';
                                        }
                                    ?>
                                    <span class="badge p-2 badge-<?= $badge ?>"><?= $value->jenis_bayar ?></span>
                                </td>
                                <td>Rp. <?= number_format($value->total) ?></td>
                            </tr>
                        <?php endforeach ; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php include 'partials/view-footer.php' ?>