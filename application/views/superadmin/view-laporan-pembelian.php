<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Laporan Pembelian</h1>

    <!-- DataTales Laporan Stok Masuk -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <a href="<?php echo base_url("laporan/export_laporan_pembelian") ?>" class="btn btn-primary btn-sm"><i class="fa fa-print"></i></a>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Nama Pelanggan</th>
                            <th>Jumlah Hutang</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_pembelian as $row) : ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->created_at; ?></td>
                            <td><?php echo $row->nama_pelanggan; ?></td>
                            <td><?php echo $row->total_hutang; ?></td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>




</div>
<?php include 'partials/view-footer.php' ?>