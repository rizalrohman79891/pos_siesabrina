<h2><center>LAPORAN PEMBAYARAN HUTANG</center></h2>
<table border="1" width="100%" style="text-align:center;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Nama Pelanggan</th>
                            <th>Nomber Faktur</th>
                            <th>Jumlah Bayar Hutang</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_laporan_bayar_hutang as $row) : ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->created_at; ?></td>
                            <td><?php echo $row->nama_pelanggan; ?></td>
                            <td><?php echo $row->no_faktur; ?></td>
                            <td>Rp. <?php echo number_format($row->total_bayar) ?></td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>

                </table>