<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Mutasi Barang</h1>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalTambahMutasiBarang">
              Tambah Mutasi Barang
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Stok</th>
                            <th>Tanggal</th>
                            <th>Jenis</th>
                            <th>Keterangan</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_mutasi_barang as $row) : ?>
                        <tr>
                            <td><?php echo $no ?></td>
                            <td><?php echo $row->nama_barang ?></td>
                            <td><?php echo $row->stok ?></td>
                            <td><?php echo $row->tanggal ?></td>
                            <td><?php echo $row->jenis ?></td>
                            <td><?php echo $row->keterangan ?></td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php include 'modal-data-mutasi-barang.php' ?>
<?php include 'partials/view-footer.php' ?>
<script>
    $(function () {

        $('.toko').hide()
        $('.gudang').hide()
        $('#jenis').attr('disabled', String(true))

        $(document).on('change', '#id_barang', function () {
            $("#jenis").val('')
            $("#stok_toko").val('')
            $("#stok_gudang").val('')
            $('.toko').hide()
            $('.gudang').hide()

            let idBarang = $(this).val()

            if(idBarang > 0) {
                $('#jenis').removeAttr('disabled', String(true))
            } else {
                $('#jenis').attr('disabled', String(true))
            }
        })

        $(document).on('change', '#jenis', function () {
            let jenis = $(this).val()
            let namaBarang = $("#id_barang").find(':selected').data('nama')
            let stokToko = $("#id_barang").find(':selected').data('stok_toko')
            let stokGudang = $("#id_barang").find(':selected').data('stok_gudang')

            if(stokToko == 0) {
                Swal.fire('Stok Barang : '+namaBarang+' di Toko Habis!', '', 'info')
            }

            if(stokGudang == 0) {
                Swal.fire('Stok Barang : '+namaBarang+' di Gudang Habis!', '', 'info')
            }

            if (jenis == 'Toko ke Gudang') {
                $('.toko').show()
                $('.gudang').hide()

                $("#stok").attr('max', stokToko)
            } else {
                $('.toko').hide()
                $('.gudang').show()
                $("#stok").attr('max', stokGudang)
            }

            $("#stok_toko").val(stokToko)
            $("#stok_gudang").val(stokGudang)
        })

        $(document).on('keyup', '#stok', function () {
            if($(this).val() > parseInt($(this).attr('max'))) {
                $(this).val($(this).attr('max'))
            } else {
                return
            }
        })

    })
</script>