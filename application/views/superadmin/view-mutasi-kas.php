<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Mutasi Kas</h1>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalTambahMutasiKas">
              Tambah Mutasi Kas
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Jumlah</th>
                            <th>Jenis</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_mutasi_kas as $row) : ?>
                        <tr>
                            <td><?php echo $no ?></td>
                            <td><?php echo $row->tanggal ?></td>
                            <td>Rp. <?= number_format($row->jumlah) ?></td>
                            <td><?= $row->jenis ?></td>
                            <td><?= $row->keterangan ?></td>
                            <td>
                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modalEditMutasiKas<?= $row->id_mutasi_kas ?>">
                                  <i class="fa fa-edit"></i>
                                </button>
                                <a href="<?php echo base_url("mutasi_kas/delete_data_mutasi_kas/").$row->id_mutasi_kas ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php include 'modal-data-mutasi-kas.php' ?>
<?php include 'partials/view-footer.php' ?>