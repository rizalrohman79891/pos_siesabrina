<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Laporan Hutang</h1>

    <!-- DataTales Laporan Stok Masuk -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <a href="<?php echo base_url("laporan/export_laporan_hutang") ?>" class="btn btn-primary btn-sm"><i class="fa fa-print"></i></a>
            <a href="<?php echo base_url("laporan/pembayaran_hutang") ?>" class="btn btn-primary btn-sm">Laporan Pembayaran Hutang</a>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal</th>
                            <th>Nama Pelanggan</th>
                            <th>Nomber Faktur</th>
                            <th>Jumlah Hutang</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_hutang as $row) : ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->created_at; ?></td>
                            <td><?php echo $row->no_faktur; ?></td>
                            <td><?php echo $row->nama_pelanggan; ?></td>
                            <td>Rp. <?php echo number_format($row->total_hutang) ?></td>
                            <td><?php echo $row->keterangan; ?></td>
                            <td>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalBayarHutang<?php echo $row->id_hutang ?>">
                                  Bayar
                                </button>
                            </td>
                        </tr>
                        <?php $no++ ?>
                        <?php endforeach; ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

    <?php $no = 1; ?>
    <?php foreach ($data_hutang as $row) : ?>
    <!-- Modal -->
    <div class="modal fade" id="modalBayarHutang<?php echo $row->id_hutang ?>" tabindex="-1" role="dialog" aria-labelledby="modalBayarHutangLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalBayarHutangLabel">Bayar Hutang</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" action="<?php echo base_url("hutang/bayar_hutang") ?>">
                <div class="form-group">
                    <label>No Faktur</label>
                    <input type="text" name="no_faktur" class="form-control" value="<?php echo $row->no_faktur ?>" readonly>
                    <input type="text" name="id_hutang" class="form-control" value="<?php echo $row->id_hutang ?>" hidden>
                </div>
                <div class="form-group">
                    <label>Nama Pelanggan</label>
                    <input type="text" name="nama_pelanggan" class="form-control" value="<?php echo $row->nama_pelanggan ?>" readonly>
                </div>
                <div class="form-group">
                    <label>Sisa Hutang</label>
                    <input type="text" name="sisa_hutang" class="form-control" value="<?php echo $row->total_hutang ?>" readonly>
                </div>
                <div class="form-group">
                    <label>Jenis Pembayaran</label>
                    <select class="form-control" name="jenis_pembayaran">
                        <option value="1">Pembayaran Pertama</option>
                        <option value="2">Pembayaran Kedua</option>
                        <option value="3">Pembayaran Ketiga</option>
                        <option value="4">Pembayaran Keempat</option>
                        <option value="5">Pembayaran Kelima</option>
                        <option value="6">Pembayaran Keenam</option>
                        <option value="7">Pembayaran Ketujuh</option>
                        <option value="8">Pembayaran Kedelapan</option>
                        <option value="9">Pembayaran Kesembilan</option>
                        <option value="10">Pembayaran Kesepuluh</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Jumlah Pembayaran</label>
                    <input type="number" name="total_bayar" class="form-control" placeholder="Masukan jumlah pembayaran">
                </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Bayar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <?php $no++ ?>
    <?php endforeach; ?>



</div>
<?php include 'partials/view-footer.php' ?>