<!-- Modal -->
<div class="modal fade" id="modalBayarPenjualan" tabindex="-1" role="dialog" aria-labelledby="modalBayarPenjualanLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group col-md-6 row">
                <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Pelanggan : </label>
                <div class="col-sm-8 col-md-8 col-xs-12">
                    <select name="id_pelanggan" id="id_pelanggan" required="" class="form-control">
                        <option value="0">-- Pilih --</option>
                        <?php $no = 1; ?>
                        <?php foreach($data_pelanggan as $key => $value) : ?>
                            <option value="<?= $value->id_pelanggan ?>"><?= $no++.' - '.$value->nama_pelanggan ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-6 row">
                <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Metode : </label>
                <div class="col-sm-8 col-md-8 col-xs-12">
                    <select name="jenis_bayar" id="jenis_bayar" required="" class="form-control">
                        <option value="CASH">Cash</option>
                        <option value="CREDIT">Credit</option>
                        <option value="TRANSFER">Transfer</option>
                    </select>
                </div>
            </div>          
        </div>
        <div class="row">
            <div class="form-group col-md-6 row">
                <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">No Faktur : </label>
                <div class="col-sm-8 col-md-8 col-xs-12">
                    <input type="text" class="form-control" name="no_faktur" value="<?= $no_faktur ?>" readonly>
                </div>
            </div>  
            <div class="form-group col-md-6 row">
                <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Total Diskon : </label>
                <div class="col-sm-8 col-md-8 col-xs-12">
                    <input type="text" class="form-control" id="totalDiskon" name="total" value="" readonly>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6 row bayar">
                <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Input Bayar : </label>
                <div class="col-sm-8 col-md-8 col-xs-12">
                    <input type="text" class="form-control" id="inputBayar" required="" name="total_bayar" value="">
                    <input type="hidden" id="inputBayarHidden">
                </div>
            </div>
            <div class="form-group col-md-6 row transfer">
                <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Transfer : </label>
                <div class="col-sm-8 col-md-8 col-xs-12">
                    <select name="id_rekening" id="id_rekening" class="form-control">
                        <option value="0">-- Pilih --</option>
                        <?php foreach($rekening as $key => $value) : ?>
                            <option value="<?= $value->id_rekening ?>"><?= $value->bank.' - '.$value->no ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-6 row">
                <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Total : </label>
                <div class="col-sm-8 col-md-8 col-xs-12">
                    <input type="text" class="form-control" id="totalBayar" name="total" value="" readonly>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6 row">
                <label class="control-label col-sm-4 col-md-4 col-xs-12" for="">Keterangan : </label>
                <div class="col-sm-8 col-md-8 col-xs-12">
                    <input type="text" class="form-control" id="keterangan" name="keterangan" value="">
                </div>
            </div>
        </div>
        <div class="row px-3 pt-3 justify-content-between">
            <h5>Kembalian Rp. <b id="kembalian">0</b></h5>
            <h5>Grand Total Rp. <b id="grandTotal">0</b></h5>
            <input type="hidden" id="kembalianDiModal">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-success" id="bayarTransaksi">Bayar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalTransaksiBarang" tabindex="-1" role="dialog" aria-labelledby="modalTransaksiBarangLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Barcode</th>
                    <th>Nama Barang</th>
                    <th>Stok</th>
                    <th>Harga</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; ?>
                <?php foreach ($data_barang as $row) : ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $row->barcode ?></td>
                    <td><?php echo $row->nama_barang ?></td>
                    <td>
                        <button class="btn btn-secondary btn-default btn-sm">Toko : <?php echo $row->stok_toko; ?></button>
                        <!-- <button class="btn btn-secondary btn-default btn-sm">Gudang : <?php echo $row->stok_gudang; ?></button> -->
                    </td>
                    <td>
                        <?php echo 'Rp. '.number_format($row->harga_jual_umum); ?>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary selectBarang" data-id="<?= $row->id_barang ?>" data-barcode="<?= $row->barcode ?>" data-nama="<?= $row->nama_barang ?>"
                        data-stok="<?= $row->stok_toko ?>" data-harga="<?= $row->harga_jual_umum ?>"><i class="fa fa-check-circle"></i></button>
                    </td>
                </tr>
                <?php $no++ ?>
                <?php endforeach; ?>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>