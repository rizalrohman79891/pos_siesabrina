<!-- Modal -->
<div class="modal fade" id="modalTambahMutasiBarang" tabindex="-1" role="dialog" aria-labelledby="modalTambahMutasiBarangLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Mutasi Kas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url("mutasi_barang/tambah_data_mutasi_barang") ?>">

          <div class="form-group">
            <label>Pilih Barang</label>
            <select class="form-control" id="id_barang" name="id_barang" required="">
                <option value="">-- Pilih --</option>
                <?php foreach($data_barang as $key => $value) : ?>
                    <option data-nama="<?= $value->nama_barang ?>" data-stok_toko="<?= $value->stok_toko ?>" data-stok_gudang="<?= $value->stok_gudang ?>"
                     value="<?= $value->id_barang ?>"><?= $value->barcode.' - '.$value->nama_barang ?></option>
                <?php endforeach; ?>
            </select>
          </div>

          <div class="form-group">
            <label>Mutasi</label>
            <select class="form-control" id="jenis" name="jenis" required="">
                <option value="">-- Pilih --</option>
                <option value="Toko ke Gudang">Toko ke Gudang</option>
                <option value="Gudang ke Toko">Gudang ke Toko</option>
            </select>
          </div>

          <div class="form-group toko">
              <label for="">Stok Asal (Toko)</label>
              <input type="text" readonly class="form-control" id="stok_toko">
          </div>
          
          <div class="form-group gudang">
            <label for="">Stok Asal (Gudang)</label>
              <input type="text" readonly class="form-control" id="stok_gudang">
          </div>

          <div class="form-group">
              <label for="">Perpindahan Jumlah Stok</label>
              <input type="number" class="form-control" name="stok" id="stok">
          </div>

          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" id="keterangan" required="" class="form-control"></textarea>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
