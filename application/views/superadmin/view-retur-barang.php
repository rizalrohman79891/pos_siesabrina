<?php include 'partials/view-header.php' ?>
<?php include 'partials/view-sidebar.php' ?>
<?php include 'partials/view-navbar.php' ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Retur Barang</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalReturPenjualan">
              <i class="fa fa-undo"></i> Retur Penjualan
            </button>
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalReturPembelian">
              <i class="fa fa-redo"></i> Retur Pembelian
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No Faktur</th>
                            <th>Supplier</th>
                            <th>Tanggal</th>
                            <th>Metode Bayar</th>
                            <th>Total</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>

</div>
<?php include 'modal-retur.php' ?>
<?php include 'partials/view-footer.php' ?>