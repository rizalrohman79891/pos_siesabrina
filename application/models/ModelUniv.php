<?php 

class ModelUniv extends CI_Model {

	public function read($table)
	{
		$result = $this->db->get($table)->result();
		return $result;
	}

	public function read_select($table, $where)
	{
		return $this->db->get_where($table, $where);

	}

	public function create($data, $table)
	{
		 $this->db->insert($table, $data);
	}

	public function delete($id, $table)
	{
		$where = "id_".$table;
		$this->db->where($where, $id);
    	$this->db->delete($table);
	}

	public function update($where, $table, $data)
	{
		$this->db->where($where);
    	$this->db->update($table, $data);
	}

	public function select_data_terbaru_with_order($table, $order)
	{
		$result = $this->db->order_by('id_pembelian_detail', $order)->get($table)->result();
		return $result;
	}

	public function join_laporan_stok_masuk()
	{
		$this->db->select('*');
		$this->db->from('pembelian_detail');
		$this->db->join('barang', 'barang.id_barang = pembelian_detail.id_barang');
		// $this->db->order_by('id_pembelian_detail', 'DESC');

		$query = $this->db->get()->result();
		return $query;
	}

	public function join_laporan_stok_keluar()
	{
		$this->db->select('*');
		$this->db->from('penjualan_detail');
		$this->db->join('barang', 'barang.id_barang = penjualan_detail.id_barang');
		// $this->db->order_by('id_pembelian_detail', 'DESC');

		$query = $this->db->get()->result();
		return $query;
	}

	public function ambil_data_laporan_hutang()
	{
		$this->db->select('*');
		$this->db->from('hutang');
		$this->db->join('penjualan','penjualan.id_penjualan=hutang.id_penjualan');
		$this->db->join('pelanggan','pelanggan.id_pelanggan=penjualan.id_pelanggan');
		$this->db->order_by('id_hutang', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function ambil_data_laporan_bayar_hutang()
	{
		$this->db->select('*');
		$this->db->from('hutang');
		$this->db->join('penjualan','penjualan.id_penjualan=hutang.id_penjualan');
		$this->db->join('pelanggan','pelanggan.id_pelanggan=penjualan.id_pelanggan');
		$this->db->join('bayar_hutang','bayar_hutang.id_hutang=hutang.id_hutang');
		$this->db->order_by('id_bayar_hutang', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function ambil_data_laporan_piutang()
	{
		$this->db->select('*');
		$this->db->from('piutang');
		$this->db->join('pembelian','pembelian.id_pembelian=piutang.id_pembelian');
		$this->db->join('supplier','supplier.id_supplier=pembelian.id_supplier');
		$query = $this->db->get();
		return $query->result();
	}

	public function ambil_data_best_seller()
	{
		$result = $this->db->order_by('id_pembelian_detail', $order)->get($table)->result();
		return $result;
	}

	public function ambil_data_penjualan_Terakhir()
	{
		$this->db->select('*');
		$this->db->from('penjualan_detail');
		$this->db->order_by('id_pembelian_detail', 'DESC');

		$query = $this->db->get()->result();
		return $query;

	}

}