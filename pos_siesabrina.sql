-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2022 at 05:21 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_siesabrina`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `barcode` varchar(30) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `harga_beli` int(11) NOT NULL DEFAULT 0,
  `harga_jual_umum` int(11) NOT NULL DEFAULT 0,
  `harga_jual_grosir` int(11) NOT NULL DEFAULT 0,
  `harga_jual_khusus` int(11) NOT NULL DEFAULT 0,
  `stok_toko` int(11) NOT NULL DEFAULT 0,
  `stok_gudang` int(11) NOT NULL DEFAULT 0,
  `kategori` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `barcode`, `nama_barang`, `harga_beli`, `harga_jual_umum`, `harga_jual_grosir`, `harga_jual_khusus`, `stok_toko`, `stok_gudang`, `kategori`, `created_at`) VALUES
(3, '1233131230', 'Helm JPN', 150000, 165000, 170000, 175000, 0, 10, 'Celana', '2022-05-11 08:33:52'),
(4, '1233131230', 'Shampoo Sunsilk', 30000, 35000, 32500, 37000, 15, 30, 'Atasan', '2022-05-16 12:25:43');

-- --------------------------------------------------------

--
-- Table structure for table `hutang`
--

CREATE TABLE `hutang` (
  `id_hutang` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `total_hutang` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hutang`
--

INSERT INTO `hutang` (`id_hutang`, `id_penjualan`, `total_hutang`, `created_at`) VALUES
(1, 2, 800000, '2022-05-16 15:20:28');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi_barang`
--

CREATE TABLE `mutasi_barang` (
  `id_mutasi_barang` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `stok` double NOT NULL,
  `tanggal` datetime NOT NULL,
  `jenis` text NOT NULL,
  `keterangan` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mutasi_barang`
--

INSERT INTO `mutasi_barang` (`id_mutasi_barang`, `id_barang`, `stok`, `tanggal`, `jenis`, `keterangan`, `created_at`) VALUES
(1, 3, 5, '2022-05-16 11:03:11', 'Gudang ke Toko', 'Ok', '2022-05-16 04:03:11'),
(2, 1, 5, '2022-05-16 18:47:43', 'Gudang ke Toko', 'Ok', '2022-05-16 11:47:43');

-- --------------------------------------------------------

--
-- Table structure for table `mutasi_kas`
--

CREATE TABLE `mutasi_kas` (
  `id_mutasi_kas` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `jumlah` double NOT NULL,
  `jenis` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mutasi_kas`
--

INSERT INTO `mutasi_kas` (`id_mutasi_kas`, `tanggal`, `jumlah`, `jenis`, `keterangan`) VALUES
(2, '2022-05-14 22:28:16', 200000, 'Pemasukkan', 'Ok');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `no_tlp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `nama_pelanggan`, `no_tlp`, `alamat`, `created_at`) VALUES
(1, 'Nandi Nugraha Santoso', '083825126384', 'Gunung Tugu, Campaka', '2022-05-11 08:52:44'),
(3, 'Farel Pratama', '0889123891', 'Campaka\r\n', '2022-05-11 09:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `no_faktur` varchar(200) NOT NULL,
  `no_referensi` varchar(200) DEFAULT NULL,
  `id_supplier` int(11) NOT NULL,
  `total` double NOT NULL,
  `diskon` double NOT NULL,
  `tanggal` datetime NOT NULL,
  `status` enum('PENDING','FAILED','SUCCESS') NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenis_bayar` enum('CASH','TRANSFER','CREDIT') NOT NULL,
  `total_bayar` double NOT NULL,
  `kembalian` double NOT NULL,
  `keterangan` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `no_faktur`, `no_referensi`, `id_supplier`, `total`, `diskon`, `tanggal`, `status`, `id_user`, `jenis_bayar`, `total_bayar`, `kembalian`, `keterangan`, `created_at`) VALUES
(9, 'SIE-5702938164', NULL, 2, 165000, 0, '2022-05-15 22:21:00', 'SUCCESS', 1, 'CREDIT', 170000, 5000, 'test', '2022-05-15 15:22:39'),
(11, 'SIE-9531086427', NULL, 3, 168000, 0, '2022-05-15 22:22:00', 'SUCCESS', 1, 'TRANSFER', 168000, 0, '', '2022-05-15 15:24:12'),
(15, 'SIE-5276384109', NULL, 3, 165000, 0, '2022-05-16 05:45:00', 'SUCCESS', 1, 'TRANSFER', 0, 0, 'Ok', '2022-05-15 22:45:21'),
(16, 'SIE-5790816243', NULL, 2, 165000, 0, '2022-05-16 05:53:00', 'SUCCESS', 1, 'CASH', 0, 0, '', '2022-05-15 22:54:09'),
(17, 'SIE-2804196753', NULL, 2, 165000, 0, '2022-05-16 05:57:00', 'SUCCESS', 1, 'CASH', 0, 0, '', '2022-05-15 22:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_detail` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `harga` double NOT NULL,
  `subtotal` double NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_detail`, `id_pembelian`, `id_barang`, `quantity`, `harga`, `subtotal`, `keterangan`, `created_at`) VALUES
(3, 9, 3, 1, 165000, 165000, '', '2022-05-15 15:22:39'),
(4, 11, 3, 1, 165000, 165000, '', '2022-05-15 15:24:12'),
(5, 11, 1, 1, 3000, 3000, '', '2022-05-15 15:24:12'),
(9, 15, 3, 1, 165000, 165000, '', '2022-05-15 22:45:21'),
(10, 16, 3, 1, 165000, 165000, '', '2022-05-15 22:54:09'),
(11, 17, 3, 1, 165000, 165000, '', '2022-05-15 22:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_transfer`
--

CREATE TABLE `pembelian_transfer` (
  `id_pembelian_transfer` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `total_transfer` double NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian_transfer`
--

INSERT INTO `pembelian_transfer` (`id_pembelian_transfer`, `id_pembelian`, `total_transfer`, `id_rekening`, `created_at`) VALUES
(1, 11, 168000, 1, '2022-05-15 15:24:12'),
(2, 15, 165000, 1, '2022-05-15 22:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `no_faktur` varchar(200) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `total` double NOT NULL,
  `total_bayar` double NOT NULL,
  `kembalian` double NOT NULL,
  `total_diskon` double NOT NULL,
  `status` enum('SUCCESS','FAILED','PENDING') NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenis_bayar` enum('CASH','TRANSFER','CREDIT') NOT NULL,
  `no_referensi` longtext NOT NULL,
  `keterangan` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_penjualan`, `no_faktur`, `id_pelanggan`, `tanggal`, `total`, `total_bayar`, `kembalian`, `total_diskon`, `status`, `id_user`, `jenis_bayar`, `no_referensi`, `keterangan`, `created_at`) VALUES
(1, 'PNJ-202205162215530273', 1, '2022-05-16 22:17:26', 300000, 0, 0, 40000, 'SUCCESS', 1, 'CASH', '', 'Oke', '2022-05-16 15:17:26'),
(2, 'PNJ-202205162219581356', 1, '2022-05-16 22:20:28', 800000, 1000000, 200000, 25000, 'SUCCESS', 1, 'CREDIT', '', 'Oke', '2022-05-16 15:20:28'),
(3, 'PNJ-202205162220285947', 0, '2022-05-16 22:21:03', 495000, 0, 0, 0, 'SUCCESS', 1, 'TRANSFER', '', 'Oke', '2022-05-16 15:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_penjualan_detail` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `harga` double NOT NULL,
  `subtotal` double NOT NULL,
  `diskon` double NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id_penjualan_detail`, `id_penjualan`, `id_barang`, `quantity`, `harga`, `subtotal`, `diskon`, `keterangan`, `created_at`) VALUES
(1, 1, 3, 1, 165000, 150000, 15000, '', '2022-05-16 15:17:26'),
(2, 1, 4, 5, 35000, 150000, 25000, '', '2022-05-16 15:17:26'),
(3, 2, 3, 5, 165000, 800000, 25000, '', '2022-05-16 15:20:28'),
(4, 3, 3, 3, 165000, 495000, 0, '', '2022-05-16 15:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_transfer`
--

CREATE TABLE `penjualan_transfer` (
  `id_penjualan_transfer` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `total_transfer` double NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan_transfer`
--

INSERT INTO `penjualan_transfer` (`id_penjualan_transfer`, `id_penjualan`, `total_transfer`, `id_rekening`, `created_at`) VALUES
(1, 3, 495000, 1, '2022-05-16 15:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `piutang`
--

CREATE TABLE `piutang` (
  `id_piutang` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `total_piutang` double NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `piutang`
--

INSERT INTO `piutang` (`id_piutang`, `id_pembelian`, `total_piutang`, `timestamp`) VALUES
(1, 9, 165000, '2022-05-15 15:22:39');

-- --------------------------------------------------------

--
-- Table structure for table `rekening`
--

CREATE TABLE `rekening` (
  `id_rekening` int(11) NOT NULL,
  `no` text NOT NULL,
  `bank` text NOT NULL,
  `pemilik` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rekening`
--

INSERT INTO `rekening` (`id_rekening`, `no`, `bank`, `pemilik`, `created_at`) VALUES
(1, '213123134123', 'BCA', 'Rizal Rochman', '2022-05-15 15:13:18');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id_sales` int(11) NOT NULL,
  `nama_sales` varchar(100) NOT NULL,
  `no_tlp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id_sales`, `nama_sales`, `no_tlp`, `alamat`, `created_at`) VALUES
(1, 'Rizal', '089518894819', 'Nagrak', '2022-05-11 09:27:50');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `no_tlp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `no_tlp`, `alamat`, `created_at`) VALUES
(2, 'Mekar Utama ', '089518894819', 'Cianjur', '2022-05-11 09:19:39'),
(3, 'TEST', '0812312321', 'TEST', '2022-05-13 14:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','kasir') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `role`) VALUES
(1, 'Rizal Rohman', 'rizal', 'ed6b202e937c3e83cfc9f9ea1aedbe0e', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`id_hutang`);

--
-- Indexes for table `mutasi_barang`
--
ALTER TABLE `mutasi_barang`
  ADD PRIMARY KEY (`id_mutasi_barang`);

--
-- Indexes for table `mutasi_kas`
--
ALTER TABLE `mutasi_kas`
  ADD PRIMARY KEY (`id_mutasi_kas`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`),
  ADD UNIQUE KEY `no_referensi` (`no_referensi`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_detail`);

--
-- Indexes for table `pembelian_transfer`
--
ALTER TABLE `pembelian_transfer`
  ADD PRIMARY KEY (`id_pembelian_transfer`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id_penjualan_detail`);

--
-- Indexes for table `penjualan_transfer`
--
ALTER TABLE `penjualan_transfer`
  ADD PRIMARY KEY (`id_penjualan_transfer`);

--
-- Indexes for table `piutang`
--
ALTER TABLE `piutang`
  ADD PRIMARY KEY (`id_piutang`);

--
-- Indexes for table `rekening`
--
ALTER TABLE `rekening`
  ADD PRIMARY KEY (`id_rekening`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id_sales`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hutang`
--
ALTER TABLE `hutang`
  MODIFY `id_hutang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mutasi_barang`
--
ALTER TABLE `mutasi_barang`
  MODIFY `id_mutasi_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mutasi_kas`
--
ALTER TABLE `mutasi_kas`
  MODIFY `id_mutasi_kas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pembelian_transfer`
--
ALTER TABLE `pembelian_transfer`
  MODIFY `id_pembelian_transfer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id_penjualan_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `penjualan_transfer`
--
ALTER TABLE `penjualan_transfer`
  MODIFY `id_penjualan_transfer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `piutang`
--
ALTER TABLE `piutang`
  MODIFY `id_piutang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rekening`
--
ALTER TABLE `rekening`
  MODIFY `id_rekening` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id_sales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
